import { useState } from 'react';
import { useQuery } from 'react-query'
import './App.css'

export interface IData {
  id: number;
  email: string;
  first_name: string;
  last_name: string;
  avatar: string;
}

async function getUser(id: number) {
  const req = await fetch(`https://reqres.in/api/users/${id}?delay=1`)

  const res = await req.json()

  if (!req.ok) {
    throw new Error(res.error)
  }

  return res.data as IData
}

function App() {
  const [currentUserId, setCurrentUserId] = useState(1)

  const { data, isLoading, isError } = useQuery(
    ["users", currentUserId, ],
    () => getUser(currentUserId)
  )

  if (isError) {
    return (
      <section>
      <p>Algo deu errado</p>
      </section>
    )
  }

  if (!data || isLoading) {
    return (
      <section>
      <p>Loading</p>
      </section>
    )
  }

  return (
    <section>
      <img src={data.avatar} alt="" />

      <p>{data.first_name} {data.last_name}</p>

      <p>{data.email}</p>

      <div>
        <button onClick={() => setCurrentUserId((prev) => prev - 1)} >Prev</button>
        <button onClick={() => setCurrentUserId((prev) => prev + 1)}>Next</button>
      </div>
    </section>
  )
}
export default App