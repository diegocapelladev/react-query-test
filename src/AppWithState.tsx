import { useEffect, useState } from 'react';
import './App.css'

export interface IData {
  id: number;
  email: string;
  first_name: string;
  last_name: string;
  avatar: string;
}

async function getUser(id: number) {
  const req = await fetch(`https://reqres.in/api/users/${id}?delay=1`)

  const res = await req.json()

  if (!req.ok) {
    throw new Error(res.error)
  }

  return res.data as IData
}

function App() {
  const [currentUserId, setCurrentUserId] = useState(1)
  const [user, setUser] = useState<IData>()
  const [loading, setLoading] = useState(false)
  const [error, setError] = useState(false)

  useEffect(() => {

    setLoading(true)

    getUser(currentUserId).then((res) => {
      setUser(res)
    }).catch((err) => {
      setError(true)
      setLoading(false)
    })

    setLoading(false)

  }, [currentUserId])

  if (error) {
    return (
      <section>
      <p>Algo deu errado</p>
      </section>
    )
  }

  if (!user || loading) {
    return (
      <section>
      <p>Loading</p>
      </section>
    )
  }
  
  return (
    <section>
      <img src={user.avatar} alt="" />

      <p>{user.first_name} {user.last_name}</p>

      <p>{user.email}</p>

      <div>
        <button onClick={() => setCurrentUserId((prev) => prev - 1)} >Prev</button>
        <button onClick={() => setCurrentUserId((prev) => prev + 1)}>Next</button>
      </div>
    </section> 
  )
}

export default App
